import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

// import { AuthService } from '../core/services/auth.service';
// import { ValidationService } from '../core/services/validation.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html' 
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    errorMessage: string;
    submitted = false;
    
    passwrdRegEx='^(?=.*\d).{4,8}$';
    constructor(private formBuilder: FormBuilder, private router: Router) { }

    ngOnInit() {
        this.buildForm();
    }

    buildForm() {
        this.loginForm = this.formBuilder.group({
            email:      ['', [ Validators.required,Validators.email]],
            password:   ['', [ Validators.required,Validators.maxLength(8),Validators.pattern(this.passwrdRegEx)]]
        });
    }

    get f() { return this.loginForm.controls; }

    saveUser() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.loginForm.valid) {
            this.router.navigate(['/customers']);
        }
        
       
    }
}
