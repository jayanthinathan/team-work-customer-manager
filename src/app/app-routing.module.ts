import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  // { path: '', pathMatch: 'full', redirectTo: '/customers' },
  { path: 'customers', loadChildren: './customers/customers.module#CustomersModule' },
  { path: 'orders', loadChildren: './orders/orders.module#OrdersModule' },
  { path: 'about', loadChildren: './about/about.module#AboutModule' },
  { path: 'login', loadChildren:'./login/login.module#LoginModule'},
  // { path: '**', pathMatch: 'full', redirectTo: '/customers' }
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
