import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NavbarComponent } from './navbar/navbar.component';
// import { CustomersComponent } from '../customers/customers.component';


const routes: Routes = [
    {path: '' , component:NavbarComponent},
    // {path:'/customers', component:CustomersComponent} 
  // { path: 'customers', loadChildren: './customers/customers.module#CustomersModule' },
  // { path: 'orders', loadChildren: './orders/orders.module#OrdersModule' },
  // { path: 'about', loadChildren: './about/about.module#AboutModule' },
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class CoreRoutingModule {
 }
