import { Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  
  isCollapsed: boolean;
  constructor(private route:Router) { }

  ngOnInit() {

  }
  loginOrOut()
  {
    this.route.navigateByUrl('/orders');
  }
}
