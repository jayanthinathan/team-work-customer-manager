import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
@Injectable({
  providedIn: 'root'
})
export class MyServiceService {

  constructor( private myservice : HttpClient) { }

  getlist()
  {
   return this.myservice.get('assets/customers.json')
   
  }
}
