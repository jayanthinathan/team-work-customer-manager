import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CardViewComponent } from './card-view/card-view.component';
import { ListViewComponent } from './list-view/list-view.component';
import { CustomersViewComponent } from './customers-view/customers-view.component';



const routes: Routes = [
    { path: '' , component:CustomersViewComponent,
      children:[
      { path:'card', component:CardViewComponent},
      { path:'list', component:ListViewComponent}]}
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class CustomersRoutingModule {
 }
