import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-customers-view',
  templateUrl: './customers-view.component.html',
  styleUrls: ['./customers-view.component.css']
})
export class CustomersViewComponent implements OnInit {

  title:String;
  
  constructor(private routes:Router) { }

  ngOnInit() {
    this.title = 'Customers';
  } 
  // cardView()
  // {
  //   this.routes.navigate[("/card")];
  // }
  // listView()
  // {
  //   this.routes.navigate[("/list")];
  // }
  // mapView()
  // {
  //   this.routes.navigate[("/map")];
  // }
}
