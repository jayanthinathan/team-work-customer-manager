import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomersRoutingModule } from './customers-routing.module';
import { CardViewComponent } from './card-view/card-view.component';
import { ListViewComponent } from './list-view/list-view.component';
import { CustomersViewComponent } from './customers-view/customers-view.component';
import { HttpClientModule } from '@angular/common/http';
@NgModule({
  declarations: [ 
    CardViewComponent, 
    ListViewComponent,
    CustomersViewComponent
     ],
  imports: [ 
    CommonModule,
    CustomersRoutingModule,
    HttpClientModule
     ],     
  exports: [ CardViewComponent,ListViewComponent]
})
export class CustomersModule { }
