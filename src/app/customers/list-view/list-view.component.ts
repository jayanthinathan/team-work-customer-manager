import { Component, OnInit } from '@angular/core';
import { DetailsService } from 'src/app/details.service';

@Component({
  selector: 'app-list-view',
  templateUrl: './list-view.component.html',
  styleUrls: ['./list-view.component.css']
})
export class ListViewComponent implements OnInit {
  yearFetch: any;
  constructor(private details:DetailsService) { }
  ngOnInit() {
    this.details.yearData().subscribe(results => {
      this.yearFetch = results;

      console.log(results)
      for (const year of this.yearFetch) {
          console.log(year)
          if (year && year.orders) {
            let total = 0;
            for (const order of year.orders) {
              total += order.itemCost;
            }
            year.orderTotal = total;
                }
        }
    })
  }
}
